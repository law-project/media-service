import json
from http import HTTPStatus

import werkzeug
from flask import Response
from flask_restful import Resource, reqparse
from lawms.controllers.media import MediaController
from lawms.schemas.commons import BaseErrorRensposeSchema
from lawms.schemas.media import UploadMediaSchema
from lawms.tools.decorators import validate_token


class MediaResource(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument(
            "file", type=werkzeug.datastructures.FileStorage, location="files"
        )

    @validate_token
    def get(self):
        status, data = MediaController().get_list()

        return Response(json.dumps(data), status=status, mimetype="application/json",)

    @validate_token(return_validation_data=True)
    def post(self, validation_data={}):
        kwargs = self.reqparser.parse_args()
        errors = UploadMediaSchema().validate(kwargs)
        if errors:
            data = BaseErrorRensposeSchema().dump(
                {"code": 422, "message": "Data tidak valid.", "errors": errors}
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNPROCESSABLE_ENTITY,
                mimetype="application/json",
            )

        kwargs.update(validation_data)

        status, data = MediaController(**kwargs).create()
        return Response(json.dumps(data), status=status, mimetype="application/json",)


class MediaDetailResource(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument(
            "file", type=werkzeug.datastructures.FileStorage, location="files"
        )

    @validate_token
    def get(self, id):
        kwargs = {"id": id}
        status, data = MediaController(**kwargs).get()

        return Response(json.dumps(data), status=status, mimetype="application/json",)

    @validate_token
    def delete(self, id):
        kwargs = {"id": id}
        status, data = MediaController(**kwargs).delete()

        return Response(json.dumps(data), status=status, mimetype="application/json",)

    @validate_token(return_validation_data=True)
    def put(self, id, validation_data={}):
        kwargs = self.reqparser.parse_args()
        errors = UploadMediaSchema().validate(kwargs)
        if errors:
            data = BaseErrorRensposeSchema().dump(
                {"code": 422, "message": "Data tidak valid.", "errors": errors}
            )
            return Response(
                json.dumps(data),
                status=HTTPStatus.UNPROCESSABLE_ENTITY,
                mimetype="application/json",
            )

        kwargs.update({"id": id})
        kwargs.update(validation_data)
        status, data = MediaController(**kwargs).update()
        return Response(json.dumps(data), status=status, mimetype="application/json",)

    def post(self, id):
        return self.put(id)
