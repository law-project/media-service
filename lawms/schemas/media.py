import werkzeug
from lawms import ma
from lawms.models.media import Media
from marshmallow import ValidationError, fields, validates

from .commons import BaseResponseSchema


class MediaSchema(ma.ModelSchema):
    created_at = fields.DateTime("%A, %d %B %Y %H:%M:%S")
    updated_at = fields.DateTime("%A, %d %B %Y %H:%M:%S")

    class Meta:
        model = Media
        exclude = (
            "is_deleted",
            "deleted_at",
        )


class MediaDataSchema(ma.Schema):
    media = fields.Nested(MediaSchema(many=True))
    total = fields.Integer()


class MediaListResponseSchema(BaseResponseSchema):
    data = fields.Nested(MediaDataSchema())


class UploadMediaResponseSchema(BaseResponseSchema):
    data = fields.Nested(MediaSchema)


class MediaResponseSchema(BaseResponseSchema):
    data = fields.Nested(MediaSchema)


class UploadMediaSchema(ma.Schema):
    file = fields.Raw(required=True)

    @validates("file")
    def validate_file(self, value):
        message = "File tidak sesuai"
        if not isinstance(value, werkzeug.datastructures.FileStorage):
            raise ValidationError(message)

        if not hasattr(value, "filename"):
            raise ValidationError(message)
