from datetime import datetime

from lawms import ma
from marshmallow import fields


class BaseResponseSchema(ma.Schema):
    code = fields.String()
    message = fields.String()
    timestamp = fields.DateTime(default=datetime.now())


class BaseErrorRensposeSchema(BaseResponseSchema):
    errors = fields.Raw()
