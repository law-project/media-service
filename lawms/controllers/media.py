from http import HTTPStatus
from typing import Any

from lawms.models.media import Media
from lawms.schemas.commons import BaseErrorRensposeSchema, BaseResponseSchema
from lawms.schemas.media import (
    MediaListResponseSchema,
    MediaResponseSchema,
    UploadMediaResponseSchema,
)


class MediaController:
    def __init__(self, **kwargs):
        super().__init__()
        self.file = kwargs.get("file")
        self.id = kwargs.get("id")
        self.user_id = kwargs.get("user_id")

    def get(self) -> (HTTPStatus, Any):
        base_media_query = Media.base_query()
        media = base_media_query.filter_by(id=self.id).first()

        if not isinstance(media, Media):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": "Data tidak valid.",
                    "errors": {"file": ["File tidak ditemukan."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        data = MediaResponseSchema().dump(
            {"data": media, "code": 200, "message": "Success get list media"}
        )

        return HTTPStatus.OK, data

    def get_list(self) -> (HTTPStatus, Any):
        base_media_query = Media.base_query()
        total_media = base_media_query.count()
        media = base_media_query.all()

        data = MediaListResponseSchema().dump(
            {
                "data": {"media": media, "total": total_media},
                "code": "200",
                "message": "Success get list media",
            }
        )

        return HTTPStatus.OK, data

    def create(self) -> (HTTPStatus, Any):
        base_media_query = Media.base_query()
        file_exist = (
            base_media_query.filter_by(file_name=getattr(self.file, "filename")).first()
            is not None
        )
        if file_exist:
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": "Data tidak valid.",
                    "errors": {"file": ["File sudah ada."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        media = Media(
            file_name=getattr(self.file, "filename"), user_id=self.user_id
        ).save()
        if not isinstance(media, Media):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "500",
                    "message": f"Upload file gagal.",
                    "errors": {"file": ["Upload file gagal."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        data = UploadMediaResponseSchema().dump(
            {"code": 200, "message": "Upload file berhasil.", "data": media}
        )

        return HTTPStatus.OK, data

    def update(self) -> (HTTPStatus, Any):
        base_media_query = Media.base_query()
        media = base_media_query.filter_by(id=self.id).first()
        if not isinstance(media, Media):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": "Data tidak valid.",
                    "errors": {"file": ["File tidak ditemukan."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        if media.user_id != self.user_id:
            data = BaseResponseSchema().dump({"code": "403", "message": "Forbidden."})
            return HTTPStatus.FORBIDDEN, data

        media.file_name = getattr(self.file, "filename")
        media.save()

        data = UploadMediaResponseSchema().dump(
            {"code": 200, "message": "Upload file berhasil.", "data": media}
        )

        return HTTPStatus.OK, data

    def delete(self):
        base_media_query = Media.base_query()
        media = base_media_query.filter_by(id=self.id).first()
        if not isinstance(media, Media):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "422",
                    "message": "Data tidak valid.",
                    "errors": {"file": ["File tidak ditemukan."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        media = media.delete()
        if not isinstance(media, Media):
            data = BaseErrorRensposeSchema().dump(
                {
                    "code": "500",
                    "message": f"Delete file gagal.",
                    "errors": {"file": ["Upload file gagal."]},
                }
            )
            return HTTPStatus.UNPROCESSABLE_ENTITY, data

        data = BaseResponseSchema().dump(
            {"code": 200, "message": "File berhasil dihapus."}
        )

        return HTTPStatus.OK, data
