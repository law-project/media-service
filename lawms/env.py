"""
This file used to handle environment variables from os
"""
from functools import partial
from os import getenv
from typing import Any, Callable


def get_env(
    prefix: str, env_name: str, default: Any, var_type: Callable[[str], Any] = str
) -> Any:
    """
    This method used get value from os environment variables and cast to given var type,
    if value not found return the default value given

    Arguments:
        prefix {str} -- [the prefix of the environment variable]
        env_name {str} -- [the environment variable name]
        default {Any} -- [the default value if no value found in os env variable]

    Keyword Arguments:
        var_type {Callable[[str], Any]} -- [the variable type to cast return value] (default: {str})

    Returns:
        Any -- [The environment variable value]
    """
    value = getenv(f"{prefix}_{env_name}")

    if not value:
        return var_type(default)

    return var_type(value)


_str = partial(get_env, var_type=str)


def _bool(prefix: str, varname: str, default: bool = None) -> bool:
    """
    This method used to cast get_env to boolean type

    Arguments:
        prefix {str} -- [The prefix of the environment variables]
        varname {str} -- [The environment variable name]

    Keyword Arguments:
        default {bool} -- [The default value that returned if the environment variable not found] (default: {None})

    Returns:
        bool -- [The environment variable value]
    """
    return get_env(prefix, varname, default, lambda x: x in ["1", "true", "t", True])


def _int(prefix: str, varname: str, default: int = None) -> int:
    """This method used to cast get_env to integer type

    Arguments:
        prefix {str} -- [The prefix of the environment variables]
        varname {str} -- [The environment variable name]

    Keyword Arguments:
        default {int} -- [The default value that returned if the environment
                          variable not found] (default: {None})

    Returns:
        int -- [The environment variable value]
    """
    return get_env(prefix, varname, default, int)


class EnvConfig:
    """
    This file used to handle environment variable object that store the
    app_prefix value that make this object reusable for given app_prefix
    """

    def __init__(self, app_prefix: str):
        """
        The init method of class EnvConfig to set object parameters

        Arguments:
            app_prefix {str} -- [The prefix off the object environment variables]
        """
        self.app_prefix = app_prefix

    def boolean(self, varname: str, default: Any = None) -> bool:
        """
        The method to get environment variables that casted to boolean

        Arguments:
            varname {str} -- [The environment variable name]

        Keyword Arguments:
            default {Any} -- [The default value if environment variable not found] (default: {None})

        Returns:
            bool -- [Environment variable value that casted to boolean]
        """
        return _bool(self.app_prefix, varname, default)

    def string(self, varname: str, default: str = None) -> str:
        """
        The method to get environment variables that casted to string

        Arguments:
            varname {str} -- [The environment variable name]

        Keyword Arguments:
            default {Any} -- [The default value if environment variable not found] (default: {None})

        Returns:
            bool -- [Environment variable value that casted to string]
        """
        return _str(self.app_prefix, varname, default)

    def int(self, varname: str, default: int = None) -> int:
        """
        The method to get environment variables that casted to integer

        Arguments:
            varname {str} -- [The environment variable name]

        Keyword Arguments:
            default {Any} -- [The default value if environment variable not found] (default: {None})

        Returns:
            bool -- [Environment variable value that casted to integer]
        """
        return _int(self.app_prefix, varname, default)
