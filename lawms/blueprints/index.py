"""
This file handle index's blueprints
"""
from flask import Blueprint, Response
from lawms.tools.commons import make_json_response

index_blueprint = Blueprint("index", __name__, url_prefix="/")


@index_blueprint.route("", methods=("GET",))
def index() -> Response:
    """
    This method response hello world in / path

    Returns:
        [Response] -- [flask Response object]
    """

    return make_json_response(http_status=200, data={"message": "Hello, World!"})
