"""
This file handle users's blueprints
"""
from flask import Blueprint
from flask_restful import Api
from lawms.resources.media import MediaDetailResource, MediaResource

media_blueprint = Blueprint("Media", __name__, url_prefix="/media/")

media_resources = Api(media_blueprint)
media_resources.add_resource(MediaResource, "/")
media_resources.add_resource(MediaDetailResource, "<int:id>/")
