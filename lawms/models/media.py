import sqlalchemy as sa

from .commons import BaseModel


class Media(BaseModel):
    __tablename__ = "media"

    user_id = sa.Column(sa.Integer(), nullable=False)
    file_name = sa.Column(sa.Text(), nullable=False)
    compressed_file_name = sa.Column(sa.Text())
