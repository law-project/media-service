# flake8: noqa
import functools

import requests
from flask import current_app as app
from flask import request
from lawms.schemas.commons import BaseErrorRensposeSchema

from .commons import make_json_response


def validate_token(_func=None, *, return_validation_data=False):
    def decorator_validate_token(func):
        @functools.wraps(func)
        def wrapper_validate_token(*args, **kwargs):
            is_success = False
            response_message = ""
            token_validation = dict()
            try:
                auth_header = request.headers.get("Authorization")
                if not auth_header:
                    raise Exception("Unauthorized")

                auth_data = auth_header.split(" ")
                if len(auth_data) != 2:
                    raise Exception("Unauthorized")

                if auth_data[0] != "Bearer":
                    raise Exception("Unauthorized")

                validate_url = "{}/auth/validate/".format(app.config.get("CAS_URL"))

                headers = {"Authorization": auth_header}

                validate_resp = requests.post(url=validate_url, headers=headers)
                validate_status = validate_resp.status_code == 200

                if not validate_status:
                    raise Exception("Unauthorized")

                token_validation = {
                    "user_id": validate_resp.json().get("data").get("id"),
                    "user_email": validate_resp.json().get("data").get("email"),
                }

                is_success = True

            except Exception:
                response_message = "Unauthorized"
                is_success = False

            if is_success:
                if return_validation_data:
                    kwargs.update({"validation_data": token_validation})

                return func(*args, **kwargs)
            else:
                data = BaseErrorRensposeSchema().dump(
                    {
                        "code": 401,
                        "message": response_message,
                        "errors": {"Authorization": ["Invalid token."]},
                    }
                )
                return make_json_response(http_status=401, data=data)

        return wrapper_validate_token

    if _func is None:
        return decorator_validate_token
    else:
        return decorator_validate_token(_func)
